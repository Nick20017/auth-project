<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('/auth');
});

// Returns main auth page from controlle
Route::get('/auth', [AuthController::class, 'index']);

// Post request to send a code and CheckEmail middleware is applied to i
Route::post('/auth/send-code', [AuthController::class, 'post'])->middleware('check-email');

// Post request to confirm email and CheckCode middleware is applied to it
Route::post('/auth/confirm-code/{id}', [AuthController::class, 'put'])->middleware('check-code');
