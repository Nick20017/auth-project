<!DOCTYPE html>
<html>
<head>
    <!-- URL::asset generates absolute path to file -->
    <link href="{{ URL::asset('css/app.css'); }}" rel="stylesheet" type="text/css" />
    <title>Auth Project - Auth</title>
</head>
<body>
    <header id="header">
        <img src="https://img.freepik.com/darmowe-psd/makieta-logo-na-szarej-scianie_35913-2122.jpg?w=2000" alt="logo" />
        <h1>Auth Project - Auth</h1>
    </header>

    <section id="body">
        <!-- Check if session variable status is set, then displays a block -->
        @if (session('status'))
        <div id="message">
        <!-- Check if status is success, then displays a script tag that shows message about successful operation -->
            @if (session('status') == 'success')
                <script type="text/javascript">
                    alert("Congratulations,\nYou successfully logged in!");
                </script>
            @endif
        </div>
        <!-- @endif closes if statement block -->
        @endif
        <div id="form">
            <!-- If variable error is set, displays a message -->
            @if (session('error'))
            <h2 class="error">{{ session('error') }}</h2>
            @endif
            
            <!-- If email variable is set, display a form to confirm code with read-only email field -->
            @if (session('email'))
            <form id="code-submit-form" action="{{ url('/auth/confirm-code', [session('userID')]) }}" method="post">
                <!-- For proper request handling -->
                @csrf
                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" value="{{ session('email') }}" readonly />
                <label for="code">Code</label>
                <input type="text" placeholder="1234" id="code" name="code" />
                <button type="submit">Confirm code</button>
            </form>
            <!-- Otherwise display a form with email input only -->
            @else
            <form id="email-submit-form" action="{{ url('/auth/send-code') }}" method="post">
                @csrf
                <label for="email">E-mail</label>
                <input type="email" placeholder="stevejobs@gmail.com" id="email" name="email" />
                <button type="submit">Send code</button>
            </form>
            @endif
        </div>
    </section>
</body>
</html>
