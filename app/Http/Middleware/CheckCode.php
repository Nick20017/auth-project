<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

// Middleware is used to process data halfway to certain endpoint
class CheckCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Checks the length of code string, if it's less than 4, returns an erro
        if (strlen($request->code) < 4)
            return redirect('/auth')->with('error', 'Invalid code!')->with('email', $request->email);

        // Otherwise processes to the current endpoint
        return $next($request);
    }
}
