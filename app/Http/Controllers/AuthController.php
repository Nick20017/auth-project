<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Mailjet\Resources;
use App\Models\User;

class AuthController extends Controller
{
    // Returns the main page 
    public function index() {
        return view('index');
    }

    // Function is used to register user, accepts request 
    public function post(Request $request) {
        // Creates new object from User model 
        $user = new User();
        // Set email and generates confirmation code 
        $user->email = $request->email;
        $user->confirm_code = rand(1000, 10000);

        // Mailjet client is used to send an email 
        $mj = new \Mailjet\Client('3e8f80438510ac3de85f9aa9bbda00dc','17146ea856947601f41c18ef947475ed',true,['version' => 'v3.1']);
        $data = [
            'Messages' => [
              [
                'From' => [
                  'Email' => "mine-modsgames@hotmail.com",
                  'Name' => "Auth Project"
                ],
                'To' => [
                  [
                    'Email' => $request->email,
                    'Name' => $request->email
                  ]
                ],
                'Subject' => "Confirm code",
                'TextPart' => "",
                'HTMLPart' => '<h1 style="text-align: center">' . $user->confirm_code . '</h1>',
                'CustomID' => "AppGettingStartedTest"
              ]
            ]
          ];
        // Send an email 
        $res = $mj->post(Resources::$Email, ['body' => $data]);
        // Save user data in a database 
        $user->save();
      // Redirects back to auth pagr with some session data
        return redirect('/auth')->with('email', $request->email)->with('userID', $user->id);
    }

    // Used to confirm an email, accepts request and user id 
    public function put(Request $request, $id) {
        // Finds user by specified id 
        $user = User::find($id);
        // Check whether user entered correct code
        if ($user->confirm_code == $request->code) {
          // Reset confirm code 
            $user->confirm_code = null;
            // Mark that email is confirmed 
            $user->is_confirmed = 1;
            // Save update
            $user->save();
            // Redirect to auth page with status of success
            return redirect('/auth')->with('userID', $user->id)->with('status', 'success');
        }
        // Otherwise redirect back to log in page with error message 
        return redirect('/auth')->with('email', $request->email)->with('userID', $user->id)->with('error', "You've entered wrong confirmation code!\nTry again.");
    }
}
