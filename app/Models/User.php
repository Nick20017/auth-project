<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// Represents a db tabl
class User extends Model
{
    use HasFactory;

    // Sets table nam
    protected $table = 'users';
    // Sets primary key
    protected $primaryKey = 'id';
}
